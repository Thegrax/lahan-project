package com.upnvj.lahan.auth;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.upnvj.lahan.R;
import com.upnvj.lahan.homepage.tab.TabActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    //inisasi Edittext dan button
    Button BtnLoginLogin, BtnLoginGmail;
    EditText EdtEmailLogin, EdtPasswordLogin;
    FirebaseAuth auth;
    String getEmail, getPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        BtnLoginLogin = (Button) findViewById(R.id.btn_login_login);
        BtnLoginLogin.setOnClickListener(this);
        EdtEmailLogin = (EditText) findViewById(R.id.edt_email_login);
        EdtPasswordLogin = (EditText) findViewById(R.id.edt_password_login);
        BtnLoginGmail = (Button) findViewById(R.id.btn_login_google);
        auth = FirebaseAuth.getInstance();

        TextView TvlupaPass = (TextView) findViewById(R.id.tv_forgot_password_login);
        TvlupaPass.setOnClickListener(this);
        TextView TvRegister = (TextView) findViewById(R.id.tv_register_right_login);
        TvRegister.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_register_right_login:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                break;
            case R.id.btn_login_login: {
                //mendapatkan data yang diinput user
                getEmail = EdtEmailLogin.getText().toString();
                getPassword = EdtPasswordLogin.getText().toString();
                //cek email kosong atau tidak
                if (TextUtils.isEmpty(getEmail) || TextUtils.isEmpty(getPassword)) {
                    Toast.makeText(this, "Email atau Sandi Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                } else {
                    loginUserAccount();
                }
                break;
            }
            case R.id.btn_login_google: {

            }
            case R.id.tv_forgot_password_login:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;
        }
    }

    //Method ini digunakan untuk proses autentikasi user menggunakan email dan kata sandi
    private void loginUserAccount() {
        auth.signInWithEmailAndPassword(getEmail, getPassword).addOnCompleteListener(task -> {
            //Mengecek Statu keberhasilan login
            if (task.isSuccessful()) {
                Toast.makeText(LoginActivity.this, "Login Sukses", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(LoginActivity.this, TabActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            } else {
                Toast.makeText(LoginActivity.this, "Tidak Dapat Masuk, Silakan Coba Lagi", Toast.LENGTH_SHORT).show();
            }
        });
    }
}