package com.upnvj.lahan.auth;

public class Account_users {
    String email;
    String nim;
    String name;
    String jurusan;
    String uid;
    String wa;



    public Account_users(String email, String name, String nim, String jurusan, String uid, String wa) {
        this.email =  email;
        this.name = name;
        this.nim = nim;
        this.jurusan = jurusan;
        this.uid = uid;
        this.wa = wa;
    }
    // method Setter Getter

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public  String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getWa() {
        return wa;
    }

    public void setWa(String wa) {
        this.wa = wa;
    }
}
