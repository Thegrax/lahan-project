package com.upnvj.lahan.auth;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.upnvj.lahan.R;

public class RegisterActivity extends AppCompatActivity {
    EditText EdtEmail, EdtName, EdtNim, EdtPassword,EdtWhatsapp;
    Button BtnRegisterRegister;
    FirebaseAuth firebaseAuth;

    String getEmail, getPassword, getName, getNim, getJurusan,getWa;
    Spinner spinner;


    //Variabel menyimpan ID dari user yang terutentikasi
    String getUserID;
    FirebaseDatabase database;
    //Kode Permintaan
    int RC_SIGN_IN = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        BtnRegisterRegister = (Button) findViewById(R.id.btn_register_register);
        EdtEmail = (EditText) findViewById(R.id.edt_email_register);
        EdtName = (EditText) findViewById(R.id.edt_name_register);
        EdtNim = (EditText) findViewById(R.id.edt_nim_register);
        EdtPassword = (EditText) findViewById(R.id.edt_password_register);
        EdtWhatsapp = (EditText)findViewById(R.id.edt_wa_register);

        //dropdown fakultas
        spinner = (Spinner) findViewById(R.id.spinner_faculty_register);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.fakultas_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        firebaseAuth = FirebaseAuth.getInstance();//Mendapatkan Instance Firebase Auth
         //Mendapatkan Instance Firebase Realtime Database
        FirebaseApp.initializeApp(this);
        getUserID = firebaseAuth.getUid();

        BtnRegisterRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cekDataUser();
            }
        });
    }

    private void cekDataUser() {
        getEmail = EdtEmail.getText().toString().trim();
        getName = EdtName.getText().toString().trim();
        getNim = EdtNim.getText().toString().trim();
        getPassword = EdtPassword.getText().toString().trim();



        if (TextUtils.isEmpty(getEmail) && TextUtils.isEmpty(getPassword) || TextUtils.isEmpty(getName) || TextUtils.isEmpty(getNim)) {
            Toast.makeText(this, "Email atau Sandi Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            //Mengecek panjang karakter password baru yang akan didaftarkan
            if (getPassword.length() < 6) {
                Toast.makeText(this, "Sandi Terlalu Pendek, Minimal 6 Karakter", Toast.LENGTH_SHORT).show();
            } else {
                createUserAccount();
            }
        }
    }

    private void createUserAccount() {
        firebaseAuth.createUserWithEmailAndPassword(getEmail, getPassword)
                .addOnCompleteListener(task -> {
                    //Mengecek status keberhasilan saat medaftarkan email dan sandi baru
                    if(task.isSuccessful()){
                        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                        String uid = currentUser.getUid();
                        getEmail = EdtEmail.getText().toString().trim();
                        getName = EdtName.getText().toString().trim();
                        getNim = EdtNim.getText().toString();
                        getJurusan = spinner.getSelectedItem().toString();
                        getWa = EdtWhatsapp.getText().toString();
                        DatabaseReference mDatabase = FirebaseDatabase.getInstance("https://lahan-a1461-default-rtdb.asia-southeast1.firebasedatabase.app/").getReference();

                        //mDatabase.child("Users").child(uid).child("Account").setValue("uid",uid);

                        mDatabase.child("Users").child(uid).child("Account").setValue(new Account_users(getEmail,getName,getNim,getJurusan,getUserID,getWa)).addOnSuccessListener(aVoid -> {
                            Toast.makeText(RegisterActivity.this, "Sign Up Success", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        });
                    }else {
                        Toast.makeText(RegisterActivity.this, "Terjadi Kesalahan, Silakan Coba Lagi", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}