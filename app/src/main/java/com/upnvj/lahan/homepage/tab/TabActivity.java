package com.upnvj.lahan.homepage.tab;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.upnvj.lahan.R;
import com.upnvj.lahan.auth.Account_users;
import com.upnvj.lahan.auth.AuthActivity;
import com.upnvj.lahan.auth.LoginActivity;
import com.upnvj.lahan.homepage.profile.ProfileActivity;
import com.upnvj.lahan.homepage.tab.drawer.ProdukActivity;

import org.jetbrains.annotations.NotNull;

public class TabActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private AppBarConfiguration mAppBarConfiguration;
    TextView NavNameTv,NavEmailTv;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener authListener;



    private final int[] TAB_TITLES = new int[]{
            R.string.tab_text_1,
            R.string.tab_text_2,
            R.string.tab_text_3
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);


        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this);
        ViewPager2 viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        new TabLayoutMediator(tabs, viewPager,
                (tab, position) -> tab.setText(getResources().getString(TAB_TITLES[position]))
        ).attach();

        if(getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0);
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);




        toggle = new ActionBarDrawerToggle(this,drawerLayout, toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));

        /* title nav header*/
        auth = FirebaseAuth.getInstance();
        View header= navigationView.getHeaderView(0);
        NavNameTv = (TextView) header.findViewById(R.id.tv_nav_nama);
        NavEmailTv = (TextView) header.findViewById(R.id.tv_nav_email);
        FirebaseUser currentUser = auth.getCurrentUser();
        String uid = currentUser.getUid();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance("https://lahan-a1461-default-rtdb.asia-southeast1.firebasedatabase.app/").getReference("Users");
        mDatabase.child(uid).child("Account").child("name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String playerName = dataSnapshot.getValue(String.class);
                NavNameTv.setText(playerName);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mDatabase.child(uid).child("Account").child("email").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String playerName = dataSnapshot.getValue(String.class);
                NavEmailTv.setText(playerName);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /* -----------------------------------------------*/
        authListener = firebaseAuth -> {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if(user == null){
                Toast.makeText(TabActivity.this, "Logout Success", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(TabActivity.this, LoginActivity.class));
                finish();
            }
        };

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        if (searchManager != null) {
            SearchView searchView = (SearchView) (menu.findItem(R.id.search)).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setQueryHint(getResources().getString(R.string.search_hint));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    Toast.makeText(TabActivity.this, query, Toast.LENGTH_SHORT).show();
                    return true;
                }
                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
        }
        return true;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START);
        Fragment fragment = null;
        int id = item.getItemId();
            switch (id) {
                case R.id.nav_home:
                    break;
                case R.id.nav_produk:
                    Intent i = new Intent(TabActivity.this, ProdukActivity.class);
                    startActivity(i);
                    break;
                case R.id.nav_profile:
                    Intent intent = new Intent(TabActivity.this, ProfileActivity.class);
                    startActivity(intent);
                    break;
                case R.id.nav_logout:
                    auth.signOut();
                    Intent intentAuth = new Intent(TabActivity.this, AuthActivity.class);
                    startActivity(intentAuth);
                    finish();
                    break;
                default:
                    break;

            }
        return true;
    }
}