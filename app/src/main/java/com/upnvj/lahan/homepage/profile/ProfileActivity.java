package com.upnvj.lahan.homepage.profile;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.upnvj.lahan.R;

public class ProfileActivity extends AppCompatActivity {
    EditText EdtName,EdtWa,EdtNim;
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener authListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Spinner spinner= (Spinner)findViewById(R.id.spinner_faculty_profile);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.fakultas_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        EdtName= findViewById(R.id.edt_name);
        EdtWa = findViewById(R.id.edt_wa);
        EdtNim= findViewById(R.id.edt_nim);
        auth = FirebaseAuth.getInstance();

        FirebaseUser currentUser = auth.getCurrentUser();
        String uid = currentUser.getUid();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance("https://lahan-a1461-default-rtdb.asia-southeast1.firebasedatabase.app/").getReference("Users");
        mDatabase.child(uid).child("Account").child("name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String playerName = dataSnapshot.getValue(String.class);
                EdtName.setText(playerName);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mDatabase.child(uid).child("Account").child("wa").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String playerName = dataSnapshot.getValue(String.class);
                EdtWa.setText(playerName);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mDatabase.child(uid).child("Account").child("nim").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String playerName = dataSnapshot.getValue(String.class);
                EdtNim.setText(playerName);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





    }
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}