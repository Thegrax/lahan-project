package com.upnvj.lahan.homepage.tab.drawer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.upnvj.lahan.homepage.tab.JasaTabFragment;
import com.upnvj.lahan.homepage.tab.ProdukTabFragment;
import com.upnvj.lahan.homepage.tab.SemuaTabFragment;

public class ProdukSectionsPager extends FragmentStateAdapter  {
    public ProdukSectionsPager(AppCompatActivity activity) {
        super(activity);
    }

    @NonNull
    @org.jetbrains.annotations.NotNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragmentproduk = null;
        switch (position) {
            case 0:
                fragmentproduk = new SemuaprodukTabFragment();
                break;
            case 1:
                fragmentproduk = new ProdukprodukTabFragment();
                break;
            case 2:
                fragmentproduk = new JasaprodukTabFragment();
                break;
        }
        return fragmentproduk;
    }

    @Override
    public int getItemCount() {
        return 3;
    }


}

