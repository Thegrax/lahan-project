package com.upnvj.lahan.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.upnvj.lahan.R;
import com.upnvj.lahan.auth.AuthActivity;
import com.upnvj.lahan.homepage.tab.TabActivity;

public class SplashScreenActivity extends AppCompatActivity {
    FirebaseAuth auth;
    FirebaseUser user;
    private static int TIMER = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        new Handler(Looper.myLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (user != null){
                    Intent intent = new Intent(SplashScreenActivity.this, TabActivity.class);
                    startActivity(intent);
                    finish();
                } else{
                    Intent intent = new Intent(SplashScreenActivity.this, AuthActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, TIMER);
    }
}